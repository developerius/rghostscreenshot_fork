#include "boxtextgraphicsitem.h"

#include <QGraphicsTextItem>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QFont>
#include <QDebug>
#include <QKeyEvent>
#include <QBrush>
#include <QPen>
#include <QGraphicsScene>
#include <utils/log.h>

BoxTextGraphicsItem::BoxTextGraphicsItem(QGraphicsItem *parent)
    : BoxGraphicsItem(parent)
{
    m_gi = new TextGraphicsItem(shapeItem());
    m_gi->setTextInteractionFlags(Qt::TextEditorInteraction);
    //m_gi->setDefaultTextColor(QColor(69, 70, 68));
    m_gi->setFocus();
	m_gi->setTextWidth(-1);
    connect(m_gi, SIGNAL(focusGain()),
            this, SLOT(deselectItem()));
    connect(m_gi, SIGNAL(textChanged()),
            this, SLOT(adaptHeight()));
	connect(m_gi, SIGNAL(moved(QPointF)), this, SLOT(textMoved(QPointF)));
}

bool BoxTextGraphicsItem::isDynamicShape() const
{
    return false;
}

void BoxTextGraphicsItem::setColor(const QColor &color)
{
    BoxGraphicsItem::setColor(color);

	m_gi->setDefaultTextColor(color);
    QPen p = shapeItem()->pen();
	p.setColor(QColor::fromRgb(255, 255, 255,0));
    p.setWidth(7);

    shapeItem()->setPen(p);
/*
    QBrush b;
    b.setStyle(Qt::SolidPattern);
    b.setColor(QColor(255, 255, 255));

    shapeItem()->setBrush(b);*/
}

void BoxTextGraphicsItem::setFont(const QFont &font)
{
    m_gi->setFont(font);
    if (isCreated()) {
        adaptHeight();
    }
}

QFont BoxTextGraphicsItem::font() const
{
    return m_gi->font();
}

void BoxTextGraphicsItem::deselectItem()
{
    setSelected(false);
    Q_EMIT itemDeselected();
}

void BoxTextGraphicsItem::adaptHeight()
{
    QRectF r = rect();
    //r.setHeight(getBoundedHeight(r));
    //setRect(r);
	updateRect(r);
}

qreal BoxTextGraphicsItem::getBoundedHeight(const QRectF &rect) const
{
    qreal h = m_gi->boundingRect().height();
    if (rect.height() > h) {
        h = rect.height();
    }
    return h;
}

QVariant BoxTextGraphicsItem::itemChange(GraphicsItemChange change, const QVariant &value)
{	
	INFO("BoxTextGraphicsItem::itemChange "+QString(change));
    if (change == ItemSelectedHasChanged) {
        bool selected = value.toBool();
		INFO("BoxTextGraphicsItem::itemChange selected "+ QString(selected));
        if (selected) {
            m_gi->clearFocus();
        }
    }
	else if(change == ItemSceneHasChanged){
		m_gi->setFocus();
	}

    return BoxGraphicsItem::itemChange(change, value);
}

void BoxTextGraphicsItem::updateRect(const QRectF &rect)
{
    QRectF r = rect;
    if (!r.isNull() && r.isValid()) {
        if (r.height() < m_gi->boundingRect().height()) {
            r.setHeight(m_gi->boundingRect().height());
        }
		if(r.width() < m_gi->boundingRect().width()){
			r.setWidth(m_gi->boundingRect().width());
		}
        /*if (r.width() < MINTEXTWIDTH) {
            r.setWidth(MINTEXTWIDTH);
        }*/
		INFO("BoxTextGraphicsItem::updateRect "+QString::number(r.width()));
        m_gi->show();
        adaptTextItemWidth(r);
        setRect(r);
        updateHandlesPosition();
    }
}

BoxTextGraphicsItem *BoxTextGraphicsItem::getNewInstance() const
{
    BoxTextGraphicsItem *item = new BoxTextGraphicsItem();
    item->m_gi->setPlainText(m_gi->toPlainText());
    item->m_gi->setFont(m_gi->font());
    return item;
}

void BoxTextGraphicsItem::adaptTextItemWidth(const QRectF &rect)
{
    m_gi->setPos(rect.topLeft());
}

void BoxTextGraphicsItem::textMoved(const QPointF &newPos)
{	
	if(!isSelected()){
		m_gi->clearFocus();
		setSelected(true);
	}
    QRectF r = rect();
    //r.translate(newPos);
	qreal x=r.x();
	r.moveTopLeft(r.topLeft()+newPos);
	INFO("BoxTextGraphicsItem::textMoved x "+QString::number(x)+ " x1 "+QString::number(r.x()));
    updateRect(r);
}
