#ifndef TEXTGRAPHICSITEM_H
#define TEXTGRAPHICSITEM_H

#include <QGraphicsTextItem>
class QPointF;

class QFocusEvent;
class QGraphicsSceneMouseEvent;
class TextGraphicsItem;

class TextGraphicsItem : public QGraphicsTextItem
{
    Q_OBJECT

public:
    explicit TextGraphicsItem(QGraphicsItem *parent = 0);

Q_SIGNALS:
    void focusGain();
    void textChanged();
	void moved(const QPointF &newPos);

protected:
    void focusInEvent(QFocusEvent *event);
    void keyPressEvent(QKeyEvent *event);
	
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	
private:
	bool m_moving;	
	QPointF last;
};

#endif // TEXTGRAPHICSITEM_H
