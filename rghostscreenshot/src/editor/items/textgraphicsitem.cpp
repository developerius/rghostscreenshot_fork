#include "textgraphicsitem.h"

#include <QGraphicsTextItem>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QRectF>
#include <QFont>
#include <QDebug>
#include <QKeyEvent>
#include <QPen>
#include <QGraphicsScene>
#include <utils/log.h>

TextGraphicsItem::TextGraphicsItem(QGraphicsItem *parent)
    : QGraphicsTextItem(parent)
{
	
}

void TextGraphicsItem::focusInEvent(QFocusEvent *event)
{
    Q_EMIT focusGain();
    QGraphicsTextItem::focusInEvent(event);
}

void TextGraphicsItem::keyPressEvent(QKeyEvent *event)
{
    QGraphicsTextItem::keyPressEvent(event);
    Q_EMIT textChanged();
}

void TextGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	INFO("TextGraphicsItem::mousePressEvent");
    if (event->button() == Qt::LeftButton) {
        m_moving = true;
		last=event->scenePos();
	}
    QGraphicsTextItem::mousePressEvent(event);
}

void TextGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_moving = false;
    }
    QGraphicsTextItem::mouseReleaseEvent(event);
}

void TextGraphicsItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{	
    if (m_moving) {
		QPointF tmp=event->scenePos();		
		QPointF diff=tmp-last;
		last=tmp;	
        Q_EMIT moved(diff);
    } else {
        QGraphicsTextItem::mouseMoveEvent(event);
    }
}