#include "fontpropertytooleditor.h"

#include <QPushButton>
#include <QFontDialog>
#include <QFont>
#include <QSettings>

FontPropertyToolEditor::FontPropertyToolEditor(QPushButton *bttn,
                                               QObject *parent)
    : PropertyToolEditor(bttn, parent)
{
    connect(bttn, SIGNAL(clicked()),
            this, SLOT(changeFont()));

    QFont font=loadFont();
    setValue(QVariant::fromValue(font));
}

void FontPropertyToolEditor::changeFont()
{
    QFont font = value().value<QFont>();
    bool ok;
    font = QFontDialog::getFont(&ok, font);
    if (ok) {
		saveFont(font);
        changeValue(QVariant::fromValue(font));
    }
}

void FontPropertyToolEditor::saveFont(QFont& font){
	QSettings settings("rghostscreenshot", "RGhost Screenshot");
    settings.beginGroup("editor");
	settings.setValue("font-family", font.family());
	settings.setValue("font-size", font.pointSize());
	settings.setValue("font-bold", font.bold());
	settings.setValue("font-italic", font.italic());
	settings.setValue("font-kerning", font.kerning());
	settings.setValue("font-overline", font.overline());
	settings.setValue("font-underline", font.underline());
	settings.setValue("font-styleName", font.styleName());
	settings.setValue("font-strikeOut", font.strikeOut());
    settings.endGroup();	
}

QFont FontPropertyToolEditor::loadFont(){
	QFont font;
    QSettings settings("rghostscreenshot", "RGhost Screenshot");
    settings.beginGroup("editor");
    font.setFamily(settings.value("font-family", font.family()).toString());
	font.setPointSize(settings.value("font-size", 10).toInt());
	font.setBold(settings.value("font-bold", true).toBool());
	font.setItalic(settings.value("font-italic", font.italic()).toBool());
	font.setKerning(settings.value("font-kerning", font.kerning()).toBool());
	font.setOverline(settings.value("font-overline", font.overline()).toBool());
	font.setUnderline(settings.value("font-underline", font.underline()).toBool());
	font.setStyleName(settings.value("font-styleName", font.styleName()).toString());
	font.setStrikeOut(settings.value("font-strikeOut", font.strikeOut()).toBool());
    settings.endGroup();	
	return font;
}
