#include "colorpropertytooleditor.h"

#include "qcolorbutton.h"
#include <QVariant>
#include <QSettings>

ColorPropertyToolEditor::ColorPropertyToolEditor(QColorButton *btn, QObject *parent)
    : PropertyToolEditor(btn, parent)
{
    connect(btn, SIGNAL(selected(QColor)),
            this, SLOT(changeColor(QColor)));
    connect(this, SIGNAL(valueSet(QVariant)),
            this, SLOT(setColor(QVariant)));

	
    setValue(QVariant::fromValue(loadColor(btn->color())));
}

ColorPropertyToolEditor::~ColorPropertyToolEditor()
{}

void ColorPropertyToolEditor::changeColor(const QColor &color)
{
	saveColor(color);
    changeValue(QVariant(color));
}

void ColorPropertyToolEditor::setColor(const QVariant &var)
{
    QColorButton *btn = static_cast<QColorButton*>(widget());
    btn->blockSignals(true);
    btn->setColor(var.value<QColor>());
    btn->blockSignals(false);
}

void ColorPropertyToolEditor::saveColor(const QColor& color){
	QSettings settings("rghostscreenshot", "RGhost Screenshot");
    settings.beginGroup("editor");
	settings.setValue("color", color.rgba());
    settings.endGroup();	
}

QColor ColorPropertyToolEditor::loadColor(QColor def){	
    QSettings settings("rghostscreenshot", "RGhost Screenshot");
    settings.beginGroup("editor");
    QColor res=QColor::fromRgba(settings.value("color",def.rgba()).toUInt());
    settings.endGroup();	
	return res;
}