//
// ScreenCloud - An easy to use screenshot sharing application
// Copyright (C) 2014 Olav Sortland Thoresen <olav.s.th@gmail.com>
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//

#include "updater.h"
#include <QUrlQuery>


#include <cstdio>
#include <string>
#include <iostream>
#include <sstream>

struct Version
{
	int major, minor, revision, build;

	Version(const std::string& version):major(0),minor(0),revision(0),build(0)
	{
		std::sscanf(version.c_str(), "%d.%d.%d.%d", &major, &minor, &revision, &build);
		if (major < 0) major = 0;
		if (minor < 0) minor = 0;
		if (revision < 0) revision = 0;
		if (build < 0) build = 0;
	}

	bool operator < (const Version& other)
	{
		if (major < other.major)
			return true;
		if (minor < other.minor)
			return true;
		if (revision < other.revision)
			return true;
		if (build < other.build)
			return true;
		return false;
	}

	bool operator == (const Version& other)
	{
		return major == other.major 
			&& minor == other.minor 
			&& revision == other.revision 
			&& build == other.build;
	}
	
	std::string toStr(){
		std::stringstream ss;
		ss<<this;
		return ss.str();
	} 

	friend std::ostream& operator << (std::ostream& stream, const Version& ver) 
	{
		stream << ver.major;
		stream << '.';
		stream << ver.minor;
		stream << '.';
		stream << ver.revision;
		stream << '.';
		stream << ver.build;
		return stream;
	}
};


Updater::Updater(QObject *parent) :
    QObject(parent)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
    pluginManager = new PluginManager(this);
    loadSettings();
}

Updater::~Updater()
{
    delete manager;
    delete pluginManager;
}
void Updater::loadSettings()
{
    QSettings settings("rghostscreenshot", "RGhost Screenshot");
    settings.beginGroup("updates");
    notifyUpdates = settings.value("check-updates-automatically", true).toBool();
    settings.endGroup();
}


void Updater::checkForUpdates(int flag)
{
    if(flag == ForceNotification)
    {
        notifyUpdates = true;
    }else if(flag == NoNotification)
    {
        notifyUpdates = false;
    }
    QUrl appVersionUrl( "https://bitbucket.org/developerius/rghostscreenshot_fork/raw/master/builds/"+QString(OS_SHORTNAME)+"/version.xml" );
    // create request parameters
    //Send the req
	INFO("Check update "+appVersionUrl.toString());
    QNetworkRequest appUpdateCheckReq;
    appUpdateCheckReq.setUrl(appVersionUrl);
    manager->get(appUpdateCheckReq);
    //Check for plugin updates
    QUrl pluginListUrl("https://bitbucket.org/developerius/rghostscreenshot-plugins_fork/raw/master/builds/plugin-list.xml");
    QNetworkRequest pluginUpdateCheckReq;
	INFO("Check plugins update "+pluginListUrl.toString());
    pluginUpdateCheckReq.setUrl(pluginListUrl);
    manager->get(pluginUpdateCheckReq);
}
void Updater::showUpdateNotification(const QString& url)
{
    INFO(tr("There is a new verision available (") + latestVersion + ")");
    if(notifyUpdates)
    {
        //Show update message
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Update available"));
        msgBox.setIcon(QMessageBox::Information);
        QPushButton changelogBtn(tr("Changelog"));
#ifdef Q_OS_WIN
        msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setText(tr("There's a new version of RGhost Screenshot available. Do you want to download it?"));
#endif
#ifdef Q_OS_MACX
        msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setText(tr("There's a new version of RGhost Screenshot available. Do you want to download it?"));
#endif
#ifdef Q_OS_LINUX
        msgBox.setText(tr("There's a new version of RGhost Screenshot available. You can download <a href="+url+"\"https://www.screencloud.net\">it</a>."));
        msgBox.addButton(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
#endif
        msgBox.addButton(&changelogBtn, QMessageBox::HelpRole);
        changelogBtn.disconnect(); //Make sure changelog button dosen't close the dialog
        connect(&changelogBtn, SIGNAL(clicked()), this, SLOT(showChangelog()));

        int selection = msgBox.exec();
        if(selection == QMessageBox::Yes)
        {
            DownloadUpdateDialog dialog;
            dialog.startDownload(latestVersion,url);
            dialog.exec();
        }
    }
}

void Updater::showPluginUpdateNotification(QStringList plugins, QStringList urls)
{
    INFO(tr("Found updates for plugin(s): '") + plugins.join("', '") + "'.");
    if(notifyUpdates)
    {
        //Show update message
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Updates for installed plugins available"));
        msgBox.setIcon(QMessageBox::Information);
        msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setText(tr("Found updates for plugin(s): <b>") + plugins.join("</b>, <b>") + tr("</b>. Do you want to update?"));
        if(msgBox.exec() == QMessageBox::Yes)
        {
            numPluginsUpdating = plugins.count();
            QProgressDialog progressDialog(tr("Updating plugins..."), tr("Cancel"), 0, 0);
            progressDialog.setWindowTitle(tr("Updating Plugins"));
            connect(pluginManager, SIGNAL(installationProgress(int)), &progressDialog, SLOT(setValue(int)));
            connect(pluginManager, SIGNAL(installationProgress(int)), this, SLOT(progressUpdate(int)));
            connect(pluginManager, SIGNAL(installationError(QString)), &progressDialog, SLOT(close()));
            connect(pluginManager, SIGNAL(installationError(QString)), this, SLOT(pluginInstallError(QString)));
            connect(&progressDialog, SIGNAL(canceled()), this, SLOT(cancelPluginUpdate()));
            connect(this, SIGNAL(updateProgessRange(int,int)), &progressDialog, SLOT(setRange(int,int)));
            progressDialog.setWindowModality(Qt::WindowModal);
            progressDialog.show();
            pluginManager->installPlugins(urls); //Trying to install a plugin that is already installed will make it update
            while(progressDialog.isVisible())
            {
                qApp->processEvents(QEventLoop::WaitForMoreEvents);
            }
            emit pluginsUpdated();
            numPluginsUpdating = 0;
        }
    }
}

void Updater::showChangelog()
{
    ChangelogDialog changelog(latestVersion);
    changelog.exec();
}

void Updater::cancelPluginUpdate()
{
    INFO(tr("Installation canceled by user"));
    pluginManager->cancelInstallation();
}

void Updater::pluginInstallError(QString error)
{
    WARNING(tr("Failed to update plugins! ") + error);
    QMessageBox::critical(NULL, tr("Failed to update plugins!"), error);
}

void Updater::progressUpdate(int)
{
    emit updateProgessRange(0, numPluginsUpdating * 4);
}

void Updater::replyFinished(QNetworkReply *reply)
{
    QString replyText = reply->readAll();
    if(reply->error() != QNetworkReply::NoError)
    {
        //Parse servers response
        QDomDocument doc("error");
        if (!doc.setContent(replyText)) {
            //No XML to parse, user is probably disconnected
            return;
        }else
        {
            QDomElement docElem = doc.documentElement();
            QDomElement message = docElem.firstChildElement("message");
            if(!message.text().isEmpty())
            {
                QMessageBox msgBox;
                msgBox.setWindowTitle(tr("Failed to check for updates"));
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setText(tr("Failed to check for updates.\nError was: ") + message.text());
                msgBox.exec();
            }
        }
    }else if(replyText.contains("<plugins>"))
    {
		INFO("Check plugins update  "+replyText);
        //This is the plugin list
        QDomDocument doc("plugins");
        if(!doc.setContent(replyText))
        {
            WARNING(tr("Failed to get plugin list from ") + reply->request().url().toString() + tr(".\n Failed to parse reply as XML"));
            QMessageBox::warning(NULL, tr("Failed to get plugin list"), tr("Failed to get plugin list from ") + reply->request().url().toString() + tr(". Failed to parse reply as XML."));
        }
        QDomElement docElem = doc.documentElement();
        QDomNode pluginNode = docElem.firstChild();
        QStringList outdatedPlugins, urls;
        while(!pluginNode.isNull())
        {
            QString shortname = pluginNode.firstChildElement("shortname").text();
            QString version = pluginNode.firstChildElement("version").text();
			QString name=tryLocalized(pluginNode,"name");
            if(PluginManager::installedVersion(shortname) != version && PluginManager::isInstalled(shortname))
            {
                outdatedPlugins.append(name);
                urls.append(pluginNode.firstChildElement("download").text());
            }
            pluginNode = pluginNode.nextSibling();
        }
        if(!outdatedPlugins.isEmpty() && !urls.isEmpty())
        {
            showPluginUpdateNotification(outdatedPlugins, urls);
        }
    }else
    {
        //This is version.xml
        QDomDocument doc("reply");
        if (!doc.setContent(replyText)) {
            return;
        }
        QDomElement docElem = doc.documentElement();
        QDomElement versionElem = docElem.firstChildElement("current_version");
		QDomElement urlElem = docElem.firstChildElement("url");      
        latestVersion = versionElem.text();
		QString url=urlElem.text();
		Version lv(latestVersion.toStdString());
		Version cv(VERSION);
		INFO("Check update latestVersion "+latestVersion);
		bool outdated=cv<lv;
        if(outdated && notifyUpdates && !url.isEmpty())
        {
            emit newVersionAvailable(latestVersion);
            showUpdateNotification(url);
        }
        emit versionNumberRecieved(latestVersion, outdated);
    }
}

