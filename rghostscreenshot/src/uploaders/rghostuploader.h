#ifndef RGHOSTUPLOADER_H
#define RGHOSTUPLOADER_H

#include <QObject>
#include <uploaders/pythonuploader.h>
#include <plugin/pluginmanager.h>
#include <QMessageBox>
#include <utils/log.h>
#include <utils/OS.h>
#include <PythonQt.h>
#include <QtConcurrentRun>

class RGhostUploader : public PythonUploader
{
    Q_OBJECT
public:
    explicit RGhostUploader(QObject *parent = 0 );
    ~RGhostUploader();

};

#endif // RGHOSTUPLOADER_H