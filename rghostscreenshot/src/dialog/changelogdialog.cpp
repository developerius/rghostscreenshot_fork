#include "changelogdialog.h"
#include "ui_changelogdialog.h"
#include <QUrlQuery>

ChangelogDialog::ChangelogDialog(QString ver,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangelogDialog),
	version(ver)
{
    ui->setupUi(this);
    busyOverlay = new BusyOverlay();
    overlayLayout = new QHBoxLayout(ui->text_changes);
    overlayLayout->setAlignment(Qt::AlignCenter);
    overlayLayout->addWidget(busyOverlay);
    connect(&netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}

ChangelogDialog::~ChangelogDialog()
{
    delete busyOverlay;
    delete ui;
    delete overlayLayout;
}

void ChangelogDialog::showEvent(QShowEvent *e)
{
    busyOverlay->show();
    QNetworkRequest getChangelogReq;
    QUrl url = QUrl("https://bitbucket.org/realisticgroup/rghostscreenshot/raw/master/builds/"+QString(OS_SHORTNAME)+"/changelog_"+version);
	/*QUrlQuery urlQuery;
    urlQuery.addQueryItem("installed-version", VERSION);
    urlQuery.addQueryItem("os", OS_SHORTNAME);
	url.setQuery(urlQuery);*/
    getChangelogReq.setUrl(url);
    netManager.get(getChangelogReq);
}

void ChangelogDialog::replyFinished(QNetworkReply *reply)
{
    busyOverlay->hide();
    QString replyText = reply->readAll();
    if(reply->error() == QNetworkReply::NoError)
    {
        ui->text_changes->setText(replyText);
    }else
    {
        ui->text_changes->setText(reply->errorString());
    }
}
