set version=1.0.0.32
set update_channel=windows
set arch=x86
::set lang=1049
::set culture=ru-ru
set lang=1033
set culture=en-us
SET LangIDs=1033

set scripts=C:\Program Files\Microsoft SDKs\Windows\v7.1\Samples\sysmgmt\msi\scripts
set sdkBin=C:\Program Files\Microsoft SDKs\Windows\v7.1\Bin

if not exist BatchSubstitute.bat (
	echo "This script requires BatchSubstitute.bat"
	pause
	exit
)

ECHO WScript.Echo InputBox( "Enter version number", "versionNumberInput", "%version%" ) >usermessage.vbs
FOR /F "tokens=*" %%A IN ('CSCRIPT.EXE //NoLogo usermessage.vbs') DO SET versionNumberInput=%%A
set version=%versionNumberInput%
DEL usermessage.vbs

ECHO WScript.Echo InputBox( "Enter update channel", "updateChannelInput", "windows" )>usermessage.vbs
FOR /F "tokens=*" %%A IN ('CSCRIPT.EXE //NoLogo usermessage.vbs') DO SET updateChannelInput=%%A
set update_channel=%updateChannelInput%
DEL usermessage.vbs

::del ScreenCloud\ScreenCloud.exe
::copy ..\..\..\Screencloud-build-desktop-Qt_4_7_4_for_Desktop_-_MinGW_4_4__Qt_SDK__Release\release\ScreenCloud.exe ScreenCloud\ScreenCloud.exe

::Strip all binaries
cd bin
print "Stripping binaries..."
strip --strip-unneeded libPythonQt.dll
strip --strip-unneeded screencloud.exe
cd ..
::Set the version number in the wix file
copy wix\ScreenCloud.wxs wix\ScreenCloud.wxs.backup

CALL BatchSubstitute.bat "VERSIONNUMBERNOTSET" %version% wix\ScreenCloud.wxs 1> wix\ScreenCloud.wxs.temp
move wix\ScreenCloud.wxs.temp wix\ScreenCloud.wxs

CALL BatchSubstitute.bat "LANDID" %lang% wix\ScreenCloud.wxs 1> wix\ScreenCloud.wxs.temp
move wix\ScreenCloud.wxs.temp wix\ScreenCloud.wxs

set outDir=..\installers\%update_channel%\%version%
set outName=RGhostScreenshot-%version%-%arch%
mkdir %outDir%

cd wix
::Build installer
candle.exe ScreenCloud.wxs -ext WixUIExtension -ext WixUtilExtension
light.exe ScreenCloud.wixobj -ext WixUIExtension -ext WixUtilExtension -b ..\bin -cultures:%culture% -out "%outDir%\%outName%.MUI.msi"

CALL ..\BuildSetupTranslationTransform.cmd ru-ru 1049

REM Add all supported languages to MSI Package attribute
echo "embed langs %LangIDs%"
cscript "%scripts%\WiLangId.vbs" %outDir%\%outName%.MUI.msi Package %LangIDs%

cd ..
::Unset the version number
::CALL BatchSubstitute.bat "%version%" VERSIONNUMBERNOTSET wix\ScreenCloud.wxs 1> wix\ScreenCloud.wxs.temp
move wix\ScreenCloud.wxs.backup wix\ScreenCloud.wxs


::copy wix\ScreenCloud.msi %outDir%\ScreenCloud-%version%-%arch%-%lang%.msi
echo "Finished building installer"
::exit
