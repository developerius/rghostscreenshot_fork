@ECHO OFF
REM
REM Do not run this file at it's own. The Build.cmd in the same folder will call this file.
REM
 
IF EXIST "%1" = "" goto failed
IF EXIST "%2" = "" goto failed
 
SET Culture=%1
SET LangID=%2
 
SET LangIDs=%LangIDs%,%LangID%
 
ECHO Building setup translation for culture "%1" with LangID "%2"...
IF EXIST ScreenCloud.wixobj light.exe ScreenCloud.wixobj -ext WixUIExtension -ext WixUtilExtension -b ..\bin -spdb -out "%outDir%\%outName%.%Culture%.msi" -cultures:%Culture%
cscript "%scripts%\WiLangId.vbs" %outDir%\%outName%.%Culture%.msi Product %LangID%
"%sdkBin%\msitran" -g "%outDir%\%outName%.MUI.msi" "%outDir%\%outName%.%Culture%.msi" "%outDir%\%Culture%.mst"
cscript "%scripts%\wisubstg.vbs" %outDir%\%outName%.MUI.msi %outDir%\%Culture%.mst %LangID%
cscript "%scripts%\wisubstg.vbs" %outDir%\%outName%.MUI.msi
::del /Q "ReleaseDir\%MsiName%.%Culture%.msi"
::del /Q "ReleaseDir\%Culture%.mst"
goto exit
 
:failed
ECHO Failed to generate setup translation of culture "%1" with LangID "%2".
 
:exit